package body Tutorial.VCs
  with SPARK_Mode => On
is
   --begin: inconsistent
   procedure Inconsistency (X : in out Integer)
   with
      Pre  => (X = 0 and X = 1),
      Post => (X = 2)
   is
   begin
      X := 3;
   end Inconsistency;
   --end
   
   procedure Main
   is
   begin
      --begin: vc_add
      declare
         X : Integer;
      begin
         X := X + 1;
      end;
      --end
      
      --begin: vc_div
      declare
         X : Integer;
         Y : Integer;
      begin
         X := X / Y;
      end;
      --end
      
   end Main;
end Tutorial.VCs;

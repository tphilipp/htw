package body Tutorial.Max
  with SPARK_Mode => On
is
   function Max (A : in Nat_Array) return Natural
   is
      Result : Natural := A (A'First);
   begin
      for I in A'First .. A'Last loop
	 if A (I) > Result then
	    Result := A (I);
	 end if;
	 
	 pragma Loop_Invariant (for all X in A'First .. I => Result >= A (X));
      end loop;
      return Result;
   end Max;
end Tutorial.Max;

with Tutorial.Types; use Tutorial.Types;

package Tutorial.Zeroize
  with SPARK_Mode => On
is
   procedure Zeroize (A : in out Nat_Array)
   with
     Post => (for all X of A => X = 0);
end Tutorial.Zeroize;

package body Tutorial.Append
  with SPARK_Mode => On
is
   function Append (A, B : in Nat_Array) return Nat_Array
   is
      Result : Nat_Array (0 .. A'Length + B'Length - 1) := (others => 0);
      I : Integer := Result'First;
   begin
      for J in A'Range loop
	 pragma Loop_Invariant (I = J - A'First);	 
	 pragma Loop_Invariant (for all K in Result'First .. I - 1 =>
				  Result (K) = A (K + A'First));
	 
	 Result (I) := A (J);
	 I := I + 1;	      
      end loop;
      
      pragma Assert (for all I in Result'First .. A'Length - 1 =>
		       Result (I) = A (I + A'First));
      
      pragma Assert (I = A'Length);
      
      for J in B'Range loop
	 pragma Loop_Invariant (I = J - B'First + A'Length);
	 pragma Loop_Invariant (for all I in Result'First .. A'Length - 1 =>
				  Result (I) = A (I + A'First));
	 pragma Loop_Invariant (for all K in Result'First + A'Length .. I - 1 =>
				  Result (K) = B (K - A'Length + B'First));
	 
	 Result (I) := B (J);
	 I := I + 1;
      end loop;
      
      pragma Assert (for all I in Result'First .. A'Length - 1 =>
		       Result (I) = A (I + A'First));
      pragma Assert (for all I in Result'First + A'Length .. Result'Last =>
		       Result (I) = B (I - A'Length + B'First));
      
      return Result;
      
   end Append;
end Tutorial.Append;

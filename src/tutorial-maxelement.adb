package body Tutorial.MaxElement
  with SPARK_Mode => On
is
   function Max_Element_Position (A : in Nat_Array) return Integer
   is
      Result : Integer := A'First;
   begin
      for I in A'First .. A'Last loop
	 if A (I) > A (Result) then
	    Result := I;
	 end if;
	 
	 pragma Loop_Invariant (for all X in A'First .. I => A (Result) >= A (X));
	 pragma Loop_Invariant (Result in A'Range);
      end loop;
      
      return Result;
   end Max_Element_Position;
end Tutorial.MaxElement;

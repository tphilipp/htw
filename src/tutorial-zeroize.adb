package body Tutorial.Zeroize
  with SPARK_Mode => On
is
   procedure Zeroize (A : in out Nat_Array)
   is
   begin
      for I in A'First .. A'Last loop
	 pragma Loop_Invariant (for all J in A'First .. I - 1 => A (J) = 0);
	 A (I) := 0;
      end loop;
   end;
end Tutorial.Zeroize;

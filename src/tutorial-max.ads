with Tutorial.Types; use Tutorial.Types;

package Tutorial.Max
  with SPARK_Mode => On
is
   function Max (A : in Nat_Array) return Natural
   with
     Pre => A'Length > 0,
     Post => (for all X of A => X <= Max'Result);
end Tutorial.Max;

package Tutorial.Types
  with SPARK_Mode => On
is
   
   type Nat_Array is array (Integer range <>) of Natural;
   
end Tutorial.Types;

with Tutorial.Types; use Tutorial.Types;

package Tutorial.MaxElement
  with SPARK_Mode => On
is
   function Max_Element_Position (A : in Nat_Array) return Integer
   with
     Pre => A'Length > 0,
     Post => (for all I in A'Range => A (I) <= A (Max_Element_Position'Result));

end Tutorial.MaxElement;

with Tutorial.Types; use Tutorial.Types;
with Tutorial.Max; use Tutorial.Max;
with Tutorial.MaxElement; use Tutorial.MaxElement;
with Tutorial.Append; use Tutorial.Append;
with Tutorial.Zeroize; use Tutorial.Zeroize;
with Tutorial.Copy; use Tutorial.Copy;
with Tutorial.Sort; use Tutorial.Sort;

with Ada.Text_IO;

procedure Main 
is
begin
   
   Ada.Text_IO.Put_Line ("START");
   
   -- Max
   declare
      A : Nat_Array (1 .. 4) := (1, 3, 2, 2);
   begin
      Ada.Text_IO.Put_Line ((if Max (A) = 3 then "Max: OK" else "Max: Fehler"));
   end;
      
   -- Max Element Pos
   declare
      A : Nat_Array (1 .. 4) := (1, 3, 2, 2);
   begin
      Ada.Text_IO.Put_Line ((if Max_Element_Position (A) = 2 then "Max_Element_Position: OK" else "Max_Element_Position: Fehler"));
   end;
      
   -- Append
   declare
      
      A : Nat_Array (1 .. 4) := (1, 3, 2, 2);
   begin
      Ada.Text_IO.Put_Line ((if Max_Element_Position (A) = 2 then "Max_Element_Position: OK" else "Max_Element_Position: Fehler"));
   end;

   
end Main;

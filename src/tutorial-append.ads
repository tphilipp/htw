with Tutorial.Types; use Tutorial.Types;

package Tutorial.Append
  with SPARK_Mode => On
is
   function Append (A, B : in Nat_Array) return Nat_Array
   with
     Pre  => A'Length + B'Length in Integer,
     Post => Append'Result (0 .. A'Length - 1) = A and
             Append'Result (A'Length .. A'Length + B'Length - 1) = B;

end Tutorial.Append;

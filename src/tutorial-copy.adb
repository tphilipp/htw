package body Tutorial.Copy
  with SPARK_Mode => On
is
   procedure Copy (A : in Nat_Array;
		   B : in out Nat_Array;
		   Index : in Natural)
   is
      B_Copy : constant Nat_Array := B with Ghost;
   begin
      for I in A'Range loop
	 pragma Loop_Invariant (I - A'First + Index in B'Range);
	 pragma Loop_Invariant (for all J in A'First .. I - 1 => 
				  B (J - A'First + Index) = A (J));
	 pragma Loop_Invariant (for all J in A'First .. I - 1 => 
				  B (Index .. Index + (J - A'First)) = A (A'First .. J));

	 pragma Loop_Invariant (B (B'First .. Index - 1) = B_Copy (B'First .. Index - 1));
	 pragma Loop_Invariant (B (Index + A'Length .. B'Last) = B_Copy (Index + A'Length .. B'Last));
	 
	 B (I - A'First + Index) := A (I);	      
      end loop;
      
      pragma Assert (B (Index .. Index + A'Length - 1) = A (A'First .. A'Last));
      pragma Assert (B (B'First .. Index - 1) = B_Copy (B'First .. Index - 1));
   end Copy;
	
end Tutorial.Copy;

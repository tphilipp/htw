with Tutorial.Types; use Tutorial.Types;

package Tutorial.Copy
  with SPARK_Mode => On
is
   procedure Copy (A     : in     Nat_Array;
		   B     : in out Nat_Array;
		   Index : in     Natural)
   with
     Pre  => 
       -- Index muss im Array B liegen
       Index in B'Range and then
       -- Wir dürfen nicht über die Limits von B schreiben
       Index + A'Last - A'First + 1 in Integer and then
       Index + A'Length <= B'Last,
     Post => 
       -- Das Array A wurde in B kopiert und alte Werte überschrieben
       B (Index .. Index + A'Length - 1) = A (A'First .. A'Last) and
       -- Links von der Index-Position wurde das Array B nicht geändert
       B (B'First .. Index - 1) = B'Old (B'First .. Index - 1) and
       -- Rechts von der Index Position plus der Länge von A ist das Array B unverändert
       B (Index + A'Length .. B'Last) = B'Old (Index + A'Length .. B'Last);
     
end Tutorial.Copy;

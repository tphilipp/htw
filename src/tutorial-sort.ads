with Tutorial.Types; use Tutorial.Types;

package Tutorial.Sort
  with SPARK_Mode => On
is
   -- Beachte: Ein leeres Array und einelementige Arrays gelten als sortiert.
   -- Mehrere Definitionen von Sortierheit sind möglich!
      
   function Is_Sorted_2 (A : in Nat_Array) return Boolean
   is (for all I in A'First .. A'Last => 
	 (for all J in A'First .. A'Last => 
	    (if (I <= J) then A (I) <= A (J))));
   
   -- Beachte:
   -- A muss sortiert sein und es sollten keine Elemente gelöscht werden, d.h.
   -- die Arrays A und B haben eine gleiche Länge und die Anzahl von Vorkommnissen von Element in beiden Arrays sind gleich ([1, 2, 2, 3] passt nicht zu [1, 1, 2, 3])
   -- Für eine Lösung, siehe:
   -- https://blog.adacore.com/manual-proof-in-spark-2014
    
end Tutorial.Sort;

CORES=4

# Compile using runtime checks and execute code
debug:
	gprbuild -Pspark_tutorial -gnato -gnat-p -gnata
	obj/main

proof:
	gnatprove -Pspark_tutorial -j $(CORES) --no-loop-unrolling --steps=4000 -cargs  -gnato13

# Generates Why3 files
why3:
	gnatprove -Pspark_tutorial -d 


.PHONY: main proof
